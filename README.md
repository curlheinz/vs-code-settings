## Installation
1. Navigate to `~/.config/Code`, remove the folder `User` and clone this repo into the current folder with the name `User`.
2. Navigate into `User` folder and execute `cat extensions.txt | xargs -L 1 code --install-extension` to install all required extensions.

If you want to create a file which contains your installed extensions:
`code --list-extensions > extensions.txt`
